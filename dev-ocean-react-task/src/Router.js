import React from 'react';
import { Switch, Route, Router, BrowserRouter } from 'react-router-dom';

import ListBusinesses from './components/businesses/ListBusinesses';
import ViewBusiness from './components/businesses/ViewBusiness';

const MyRouter = () => (
        <Switch>
            <Route exact path='/' component={ListBusinesses} />
            <Route exact path='/businesses' component={ListBusinesses} />
            <Route exact path='/businesses/view/:id' component={ViewBusiness} />
        </Switch>
)

export default MyRouter;
