import axios from 'axios';
import { LIST_BUSINESSES, LIST_BUSINESSES_ERROR, VIEW_BUSINESS } from './types';
import { push, goBack } from 'connected-react-router'
import { errorHandler } from '../functions';


// Not used
export const listBusinesses = () => dispatch => {
    axios
    .get('https://api.myjson.com/bins/13pqgi')
    .then(res => {
        dispatch({
            type: LIST_BUSINESSES,
            payload: res.data
        })
    }).catch(error => {
        dispatch({
            type: LIST_BUSINESSES_ERROR,
            payload: (error.response && error.response.data) ? error.response.data : {message: errorHandler(error)}
        })
    });
}
