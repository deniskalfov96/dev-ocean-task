export const errorHandler = (error) => {
    let errorCode = 0;
    const customErrorMsg = "Error occurred #";
        
    // https://gist.github.com/fgilio/230ccd514e9381fafa51608fcf137253
    if (error.response) {
        errorCode = 1;
        console.log(error.response.data); //DO NOT REMOVE
        console.log(error.response.status); //DO NOT REMOVE
        console.log(error.response.headers); //DO NOT REMOVE
        /*
         * The request was made and the server responded with a
         * status code that falls out of the range of 2xx
        */
    } else if (error.request) {
        errorCode = 2;
         /*
         * The request was made but no response was received, `error.request`
         * is an instance of XMLHttpRequest in the browser and an instance
         * of http.ClientRequest in Node.js
         */
        console.log(error.request); //DO NOT REMOVE
    } else {
        errorCode = 3;
        // Something happened in setting up the request and triggered an Error
        console.log('Error', error.message); //DO NOT REMOVE
    }

    return customErrorMsg + errorCode;
}