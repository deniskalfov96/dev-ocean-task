import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { connectRouter } from 'connected-react-router'

import businessReducer from  './../reducers/businesses.reducer';
// import businessReducer from  './../reservations/settings/people-limit-time/reducers/people-limit.reducer';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  routing: routerReducer,
  businesses: businessReducer,
})

export default createRootReducer
