import { LIST_BUSINESSES, VIEW_BUSINESS } from '../actions/types';

const initialState = {
    businesses: [],
}

export default function(state = initialState, action) {
    switch(action.type) {
        case LIST_BUSINESSES:
            return {
                ...state,
                businesses: action.payload,
            }
        default:
            return state;
    }
}