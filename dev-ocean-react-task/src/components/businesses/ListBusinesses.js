import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import Pagination from "react-js-pagination";
import { push } from 'react-router-redux';
import { listBusinesses } from '../../actions/businesses.actions';
import ListEachBusiness from './ListEachBusiness';
import { ITEMS_PER_PAGE, ITEMS_PAGE_RANGE_DISPLAYED } from '../../constants'

class ListBusinesses extends Component {
    constructor(props){
        super(props);
    }
    
    componentDidMount() {
        this.props.listBusinesses();
    }

    handlePageChange = (page) => {
        this.props.dispatch(push('/businesses?page='+page))
    }

    render() {
        let businesses = this.props.businesses;
        
        // Pagination variables
        const items_count = Object.keys(businesses).length
        const current_page = this.props.page;
        const start_offset = (current_page - 1) * ITEMS_PER_PAGE;
        let start_count = 0; 

        return (
            <React.Fragment>

                <div className="project-logo">
                    <ul>
                        <li>LOGO</li>
                    </ul>
                </div>
        
                <table className="list-businesses">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>

                        {items_count > 0 ?
                            (Object.values(businesses).map((business, index) => {
                                if(index >= start_offset && start_count < ITEMS_PER_PAGE) {
                                    start_count++;
                                    return <ListEachBusiness 
                                        key={business.id} 
                                        business={business}
                                    />
                                }
                            }))
                        :
                            <tr><td colspan="100%">No records</td></tr>
                        }
                    </tbody>
                </table>

                    {items_count > ITEMS_PER_PAGE ?
                        <Pagination
                            className="pagination"
                            itemClass="page-item"
                            activePage={current_page}
                            activeClass="active"
                            linkClass="page-link"
                            prevPageText="<"
                            nextPageText=">"
                            firstPageText="<<"
                            lastPageText=">>"
                            itemsCountPerPage={ITEMS_PER_PAGE}
                            totalItemsCount={items_count}
                            pageRangeDisplayed={ITEMS_PAGE_RANGE_DISPLAYED}
                            onChange={this.handlePageChange}
                        />
                    : 
                        ''
                    }

            </React.Fragment>

        )
    }
}

const mapStateToProps = (state, dispatch) => {
    return {
        businesses: state.businesses.businesses.clients || [],
        page: Number(state.router.location.query.page) || 1
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        dispatch,
        ...bindActionCreators({ listBusinesses }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps )(ListBusinesses);
