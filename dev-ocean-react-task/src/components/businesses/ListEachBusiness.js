import React from 'react'
import { NavLink } from 'react-router-dom';

const emptySymbol = '';

const ListEachRoom = (props) => {
    return (
        <tr key={props.business.id}>
            <td>
                <NavLink to={'businesses/view/' + props.business.id}>
                    {props.business.name ? props.business.name : emptySymbol}
                </NavLink>
            </td>
            <td>
                <NavLink to={'businesses/view/' + props.business.id}>
                    {props.business.description ? props.business.description : emptySymbol}
                </NavLink>
            </td>
        </tr>
    )
}

export default ListEachRoom;