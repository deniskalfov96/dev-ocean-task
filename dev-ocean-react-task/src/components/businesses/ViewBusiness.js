import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { listBusinesses } from '../../actions/businesses.actions';

class ViewBusiness extends Component {
	constructor(props) {
		super(props)

		this.state = {
            id: this.props.match.params.id,
            name: this.props.business.name,
            description: this.props.business.description,
            phone: this.props.business.phone,
            image: this.props.business.image,
            email: this.props.business.email,
            address: this.props.business.address,
		}
	}

    componentDidMount() {
        this.props.listBusinesses();
    }

	componentWillReceiveProps(nextProps) {
		if (nextProps.business.id) {
			this.setState({
                id: nextProps.match.params.id,
                name: nextProps.business.name,
                description: nextProps.business.description,
                phone: nextProps.business.phone,
                image: nextProps.business.image,
                email: nextProps.business.email,
                address: nextProps.business.address,    
			});
		}
	}
	    
    render() {  
		return (
            <div className="view-bussiness">

                <div className="project-logo">
                    <ul>
                        <li>LOGO</li>
                    </ul>
                </div>

                <img className="bussiness-image" src={this.props.business.image}/>

                <div className="item-info-container">

                <div>
                    <h3>Address</h3>
                    <div>{this.props.business.address && this.props.business.address.number ? this.props.business.address.number : ''} {this.props.business.address && this.props.business.address.street ? this.props.business.address.street : ''}</div>
                    <div>{this.props.business.address && this.props.business.address.city ? this.props.business.address.city : ''}, {this.props.business.address && this.props.business.address.country ? this.props.business.address.country : ''} {this.props.business.address && this.props.business.address.zip ? this.props.business.address.zip : ''}</div>
                </div>

                <div>
                    <h3>Contact</h3>
                    <div>{this.props.business.phone ? this.props.business.phone : ''}</div>
                    <div>{this.props.business.email ? this.props.business.email : ''}</div>
                </div>

                <div className="nearby-places">
                    <h3>Nearby Places</h3>


                    <table className="list-nearby-businesses">
                    <tbody>
                        {this.props.allBusinesses ? 
                            this.props.allBusinesses
                            .filter(b => b.address.country == this.props.business.address.country)
                            .map((nearbyBusiness) => (
                                <tr>
                                    <td>
                                        <p>{nearbyBusiness.name ? nearbyBusiness.name : ''}</p>
                                    </td>
                                    <td>
                                        {nearbyBusiness.address && nearbyBusiness.address.number ? nearbyBusiness.address.number : ''}
                                        {nearbyBusiness.address && nearbyBusiness.address.street ? nearbyBusiness.address.street : ''}

                                        {nearbyBusiness.address && nearbyBusiness.address.city ? nearbyBusiness.address.city : ''}
                                        {nearbyBusiness.address && nearbyBusiness.address.country ? nearbyBusiness.address.country : ''}
                                        {nearbyBusiness.address && nearbyBusiness.address.zip ? nearbyBusiness.address.zip : ''}
                                    </td>
                                </tr>
                            ))
                        :
                            ''
                        }
                    </tbody>
                    </table>
                </div>

                </div>

            </div>
    	)
  	}
}

const mapStateToProps = (state, ownProps) => {
    let businessId = ownProps.match.params.id;
    
    return {
        business: state.businesses.businesses.clients ? state.businesses.businesses.clients.filter(b => parseInt(b.id) === parseInt(businessId))[0] : [],
        allBusinesses: state.businesses.businesses.clients ? state.businesses.businesses.clients : [],
	}
};

export default connect(mapStateToProps, { listBusinesses })(ViewBusiness);