import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
// import rootReducer from './reducers';

import { routerMiddleware } from 'react-router-redux';
import { createBrowserHistory } from 'history'
import createRootReducer from './reducers'

export const history = createBrowserHistory()

const InitialState = {};
const midArr = [thunk, routerMiddleware(history)]

const middleware = [
    applyMiddleware(...midArr),
    ...(window.__REDUX_DEVTOOLS_EXTENSION__ ? [window.__REDUX_DEVTOOLS_EXTENSION__()] : [])
]

const store = createStore(
    createRootReducer(history),
    InitialState, 
    compose(...middleware)
);

export default store;
